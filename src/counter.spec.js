import Counter from './counter';

let elem = null;
let elemConfig = {
  name: 'Cool counter',
  inc: 1,
  initValue: 7,
  maxVal: 10,
}
let counterCmp;

const createElement = () => {
  elem = document.createElement('div');
  elem.id = 'root';
  document.body.appendChild(elem);
};

const destroyElement = () => {
  document.body.removeChild(elem);
  elem = null;
};

const createComponent = () => {
  counterCmp = new Counter(elem, elemConfig);
};

const destroyComponent = () => {
  counterCmp = null;
};

describe('counter', function () {
  beforeEach(createElement);
  afterEach(destroyElement);

  it('should be created', () => {
    let counter = new Counter(
      elem,
      elemConfig
    );
    expect(counter).toBeDefined();
    expect(counter.name).toMatch(elemConfig.name);
    expect(counter.inc).toEqual(elemConfig.inc);
    expect(elemConfig.initValue <= elemConfig.maxVal).toBe(true);
    expect(counter.value).toEqual(elemConfig.initValue);
    expect(counter.maxVal).toEqual(elemConfig.maxVal);
  });

  it('should be created with default options', () => {
    let counter = new Counter(
      elem
    );
    expect(counter).toBeDefined();
    expect(counter.name).toMatch('');
    expect(counter.inc).toEqual(1);
    expect(counter.value).toEqual(0);
    expect(counter.maxVal).toEqual(10);
  });

  it('have throw', () => {
    expect(function () {
      new Counter();
    }).toThrow();
  });
});

describe('test initial methods', function () {
  beforeEach(function () {
    createElement();
    createComponent();
  });

  afterEach(function () {
    destroyElement();
    destroyComponent();
  });

  describe('test render', () => {
    it('should create root element', () => {
      expect(counterCmp.cmpRoot instanceof HTMLElement).toBe(true);
    });

    it('should have correct title', () => {
      let title = counterCmp.cmpRoot.getElementsByClassName('counter__title')[0].innerText;
      expect(title).toMatch(elemConfig.name);
    });

    it('should have corrent value', () => {
      let value = +counterCmp.cmpRoot.getElementsByClassName('js-val')[0].innerText;
      expect(value).toEqual(elemConfig.initValue);
    });
  });

  describe('test initEvents', () => {
    beforeEach(function() {
      destroyComponent();
    });

    it('should call initEvents', () => {
      let eventsSpy = spyOn(Counter.prototype, 'initEvents');
      createComponent();

      expect(eventsSpy).toHaveBeenCalled();
    });

    it('should call onIncrement', () => {
      let incrementSpy = spyOn(Counter.prototype, 'onIncrement').and.callThrough();
      createComponent();

      let e = new MouseEvent('click');
      let oldValue = counterCmp.value;
      counterCmp.cmpRoot.getElementsByClassName('js-increment')[0].dispatchEvent(e); // 8

      expect(incrementSpy).toHaveBeenCalled();
      expect(counterCmp.value <= counterCmp.maxVal).toBe(true);
      expect(+counterCmp.cmpRoot.getElementsByClassName('js-val')[0].innerText).toEqual(counterCmp.value);

      let nextValue = oldValue + elemConfig.inc;
           
      counterCmp.cmpRoot.getElementsByClassName('js-increment')[0].dispatchEvent(e); // 9
      nextValue += elemConfig.inc;
      counterCmp.cmpRoot.getElementsByClassName('js-increment')[0].dispatchEvent(e); // 10
      nextValue += elemConfig.inc;
      counterCmp.cmpRoot.getElementsByClassName('js-increment')[0].dispatchEvent(e); // 10 || 11 ?
      nextValue += elemConfig.inc;
      
      if (nextValue > counterCmp.maxVal) {
        nextValue = counterCmp.maxVal;
      } 

      expect(counterCmp.value).toEqual(nextValue);
    });

    it('should call onDecrement', () => {
      let decrementSpy = spyOn(Counter.prototype, 'onDecrement').and.callThrough();
      const counterCmp = new Counter(elem, {
        name: 'ASD',
        inc: 1,
        initValue: 2
      });

      let oldValue = counterCmp.value;
      let e = new MouseEvent('click');

      counterCmp.cmpRoot.getElementsByClassName('js-decrement')[0].dispatchEvent(e); // 1

      expect(decrementSpy).toHaveBeenCalled();
      expect(counterCmp.value).toEqual(oldValue - counterCmp.inc);
      expect(+counterCmp.cmpRoot.getElementsByClassName('js-val')[0].innerText).toEqual(counterCmp.value);

      counterCmp.cmpRoot.getElementsByClassName('js-decrement')[0].dispatchEvent(e); // 0
      counterCmp.cmpRoot.getElementsByClassName('js-decrement')[0].dispatchEvent(e); // 0?

      expect(counterCmp.value >= 0).toBe(true);
    });
  });

  describe('set max', () => {
    it('should set max value on click', () => {
      destroyComponent();
      let setMaxSpy = spyOn(Counter.prototype, 'onSetMax').and.callThrough();
      createComponent();

      let e = new MouseEvent('click');
      counterCmp.cmpRoot.getElementsByClassName('js-set-max')[0].dispatchEvent(e);

      expect(setMaxSpy).toHaveBeenCalled();
      expect(counterCmp.value).toEqual(counterCmp.maxVal);
      expect(+counterCmp.cmpRoot.getElementsByClassName('js-val')[0].innerText).toEqual(counterCmp.maxVal);
    });
  });

  describe('set min', () => {
    it('should set min value on click', () => {
      destroyComponent();
      let setMinSpy = spyOn(Counter.prototype, 'onSetMin').and.callThrough();
      
      let counterCmp = new Counter(elem, {
        name: 'ASD',
        inc: 1,
        initValue: 5
      });

      let e = new MouseEvent('click');
      counterCmp.cmpRoot.getElementsByClassName('js-set-min')[0].dispatchEvent(e);

      expect(setMinSpy).toHaveBeenCalled();
      expect(counterCmp.value).toEqual(1);
      expect(+counterCmp.cmpRoot.getElementsByClassName('js-val')[0].innerText).toEqual(1);
    });
  });
});